angular.module("app.service", [])

  .service("ListService", [ListServiceFunction]);

  function ListServiceFunction () {

    var myStore = window.localStorage;
    var STORE_NAME = "MyListAppDB";

    if (!myStore.getItem(STORE_NAME)) {
      myStore.setItem(STORE_NAME, "[]");
    }

    var listService = {
      privates: {
        store: [],
        init: function() {
          this.store = angular.fromJson(myStore.getItem(STORE_NAME));
        },
        getAll: function() {
          return this.store;
        },
        addToStore: function(object) {
          var dupicate = _.find(this.store, {
            label: object
          });

          if (_.isEmpty(dupicate) && object !== "") {
            this.store.push({
              label: object,
              id: idGenerator()
            });
          } else {
            return true;
          }

          function idGenerator() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for( var i=0; i < 20; i++ ) {
              text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
          }

          this.saveChanges();
          return false;
        },
        saveChanges: function(store) {
          if (!_.isUndefined(store)) {
            this.store = store;
          }
          myStore.setItem(STORE_NAME, angular.toJson(this.store));
        },
        removeById: function(id) {
          _.remove(this.store, function(o) {
            return o.id === id
          });

          this.saveChanges();
        },
        edit: function(item) {
          var update = _.find(this.store, {id: item.id});
          
          update = item;

          this.saveChanges();
        }
      },
      publics: {
        get: function() {
          return listService.privates.getAll();
        },
        add: function(label) {
          return listService.privates.addToStore(label);
        },
        remove: function(id) {
          listService.privates.removeById(id);
        },
        edit: function(item) {
          listService.privates.edit(item);
        },
        clear: function() {
          listService.privates.saveChanges([]);
        },
        updateStore: function(store) {
          listService.privates.saveChanges(store);
        }
      }
    };

    listService.privates.init();

    return listService.publics;
  }