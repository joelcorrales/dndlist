angular.module("app.controller", ["app.service"])

  .controller("ListCtrl", ["$scope", "ListService", ListCtrlFunction]);

  function ListCtrlFunction ($scope, ListService) {
    var pageSize = 10;
    $scope.selected = null;
    $scope.newone = "";
    $scope.folder = [];
    $scope.pages = [];
    $scope.from = 0;
    $scope.to = pageSize;
    $scope.currentPage = 1;
    $scope.showError = false;

    $scope.clearFocus = function clearFocus () {
      $scope.showError = false;
    }

    $scope.add = function add () {
      var infolder = _.find($scope.folder, { label: $scope.newone });
      
      if (!_.isUndefined(infolder)) {
        $scope.showError = true;
        return;
      }

      $scope.showError = ListService.add($scope.newone);
      $scope.newone = "";
      updateList();
    }
    $scope.remove = function remove (id) {
      ListService.remove(id);
      updateList();
    }
    $scope.clearAll = function clearAll () {
      ListService.clear();
      updateList();
    }
    $scope.setEditable = function edit(id) {
      $scope.editable = id;
    }
    $scope.edit = function edit (item) {
      ListService.edit(item);
      $scope.editable = undefined;
      updateList();
    }

    function updateList () {
      $scope.list = ListService.get();
      $scope.pages = new Array(Math.ceil($scope.list.length/10));
    }

    $scope.updateIndexes = function() {
      ListService.updateStore($scope.list);
    }

    $scope.showPage = function showPage (page) {
      $scope.currentPage = page+1;
      $scope.from = page*pageSize;
      $scope.to = $scope.from+pageSize;
    }

    updateList();
  }